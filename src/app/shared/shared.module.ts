import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CardCrudComponent } from './card-crud/card-crud.component';


@NgModule({
    imports: [
        FormsModule,
        CommonModule
    ],
    declarations: [CardCrudComponent],
    exports: [CardCrudComponent],

})
export class SharedModule { }